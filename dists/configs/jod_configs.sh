#!/bin/bash

################################################################################
# The John Operating System Project is the collection of software and configurations
# to generate IoT EcoSystem, like the John Operating System Platform one.
# Copyright (C) 2021 Roberto Pompermaier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

###############################################################################
# Usage:
# no direct usage, included from other scripts
#
# Example configs script called by JOD distribution scripts.
# This configuration can be used to customize the JOD distribution management
# like execution, installation, etc...
#
# Artifact: JOD Dist Template
# Version:  1.0.1
###############################################################################

# JOD_YML
# Absolute or $JOD_DIR relative file path for JOD config file, default $JOD_DIR/jod.yml
#export JOD_YML="jod_2.yml"

# JAVA_HOME
# Full path of JAVA's JVM (ex: $JAVA_HOME/bin/java)
#JAVA_HOME="/Library/Java/JavaVirtualMachines/jdk1.8.0_251.jdk/Contents/Home"

# ################################## #
# JOD AltoAdige OpenDataHub's config #
# ################################## #

# ...
export JOD_AA_ODH_STATION_TYPE="EnvironmentStation"

# ...
export JOD_AA_ODH_STATION_CODE="AB2"

# #################################### #
# JOD AltoAdige OpenDataHub's Examples #
# #################################### #

# From https://opendatahub.bz.it/datasets/mobility/environment/
#export JOD_AA_ODH_STATION_TYPE="EnvironmentStation"
#export JOD_AA_ODH_STATION_CODE="AB2"

# From https://opendatahub.bz.it/datasets/mobility/echargingstation/
#export JOD_AA_ODH_STATION_TYPE="EChargingStation"
#export JOD_AA_ODH_STATION_CODE="01a75d75-e06d-4e73-8043-08553b47f25b"

# From https://opendatahub.bz.it/datasets/mobility/parking/
#export JOD_AA_ODH_STATION_TYPE="ParkingStation"
#export JOD_AA_ODH_STATION_CODE="me:parkkellereialgund"

# From https://opendatahub.bz.it/datasets/tourism/weather/
#export JOD_AA_ODH_STATION_TYPE="MeteoStation"
#export JOD_AA_ODH_STATION_CODE="00390SF"

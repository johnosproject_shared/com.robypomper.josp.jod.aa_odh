# JOD Alto Adige OpenDataHub - 1.0

Documentation for JOD Alto Adige OpenDataHub. This JOD Distribution allow to startup and manage a JOD Agent that represent a dataset station's sensors from the Alto Adige OpenDataHub APIs.

## JOD Distribution Specs

This JOD Distribution was created and maintained as part of the John O.S. Project.

|||
|---|---|
| **Current version**    | 1.0
| **References**        | [JOD Alto Adige OpenDataHub @ JOSP Docs](https://www.johnosproject.org/docs/references/jod_dists/jod_aa_odh/)
| **Repository**        | [com.robypomper.josp.jod.aa_odh @ Bitbucket](https://bitbucket.org/johnosproject_shared/com.robypomper.josp.jod.aa_odh/)
| **Downloads**            | [com.robypomper.josp.jod.aa_odh > Downloads @ Bitbucket](https://bitbucket.org/johnosproject_shared/com.robypomper.josp.jod.aa_odh/downloads/)

This distribution can be installed on any computer. When started, it generates new ```struct.jod``` file depending on the ```configs/configs.sh``` configs. Then it starts sharing dataset station's sensors to the JOSP EcoSystem like
'EnvironmentStation', 'EChargingStation', etc...

In the ```configs/configs.sh``` file you can find and configure following properties:

| Property | Example | Description |
|----------|---------|-------------|
| **JOD_AA_ODH_STATION_TYPE** | EnvironmentStation  | The dataset station type |
| **JOD_AA_ODH_STATION_CODE** | AB2  | Required station's id (or scode) . |

_All Properties are required._

**NB:** This distribution works with AA OpenDataHub / Mobility APIs. For a complete list of station types please visit [AA OpenDataHub / Mobility](https://opendatahub.bz.it/datatype/mobility/), then to list all stations replace ```{STATION_TYPE}``` and open following url, valid station's codes are contained in ```scode``` attributes:

```
https://mobility.api.opendatahub.bz.it/v2/flat%2Cnode/{STATION_TYPE}?limit=200&distinct=true
```

## JOD Distribution Usage

### Locally JOD Instance

Each JOD Distribution comes with a set of script for local JOD Instance management.

| Command | Description |
|---------|-------------|
| Start    <br/>```$ bash start.sh```     | Start local JOD instance in background mode, logs can be retrieved via ```tail -f logs/console.log``` command |
| Stop     <br/>```$ bash stop.sh```      | Stop local JOD instance, if it's running |
| State    <br/>```$ bash state.sh```     | Print the local JOD instance state (obj's id and name, isRunning, PID...) |
| Install  <br/>```$ bash install.sh```   | Install local JOD instance as system daemon/service |
| Uninstall<br/>```$ bash uninstall.sh``` | Uninstall local JOD instance as system daemon/service |

### Remote JOD Instance

To deploy and manage a JOD instance on remote device (local computer, cloud server, object device...) please use the [John Object Remote](https://www.johnosproject.org/docs/references/tools/john_object_remote/)
tools.
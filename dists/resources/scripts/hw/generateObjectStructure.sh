#!/bin/bash

################################################################################
# The John Operating System Project is the collection of software and configurations
# to generate IoT EcoSystem, like the John Operating System Platform one.
# Copyright (C) 2021 Roberto Pompermaier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
################################################################################

# Generate a new Object's structure config file representing configured Alto Adige
# OpenDataHub datasource.
#
# Remove current struct.jod file, if any, then replace all placeholder from
# struct
# Fetch the Philips Hue Gateway until user press the physical button on the
# gateway. This script retry max for MAX_RETRY_COUNT times, after that prints
# an error message and exit the script execution.
#
# Artifact: JOD Philips Hue
# Version:  1.0
###############################################################################

JOD_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd -P)/../.."
source "$JOD_DIR/scripts/libs/include.sh" "$JOD_DIR"
source "$JOD_DIR/scripts/jod/struct/builder.sh" "$JOD_DIR"

#DEBUG=true
[[ ! -z "$DEBUG" && "$DEBUG" == true ]] && setupLogsDebug || setupLogs

setupCallerAndScript "$0" "${BASH_SOURCE[0]}"

execScriptConfigs "$JOD_DIR/scripts/jod/jod-script-configs.sh"
execScriptConfigs "$JOD_DIR/scripts/jod/errors.sh"

###############################################################################
logScriptInit

# Standard configs
JOD_STRUCT=$JOD_DIR/configs/struct.jod

logInf "Generate John Object's structure for the AA OpenDataHub station '$JOD_AA_ODH_STATION_CODE' from '$JOD_AA_ODH_STATION_TYPE' dataset..."

# Fetch device list
STATION_URL="https://mobility.api.opendatahub.bz.it/v2/tree%2Cnode/$JOD_AA_ODH_STATION_TYPE/%2A/latest?limit=200&where=scode.eq.$JOD_AA_ODH_STATION_CODE&distinct=true&timezone=UTC"
STATION_DATAPOINTS=$(curl -sS -X GET "$STATION_URL" -H "accept: application/json")
#echo $STATION_DATAPOINTS && exit

MODEL="AA OpenDataHub Station"
BRAND="Alto Adige OpenDataHub"
DESCR="The '$JOD_AA_ODH_STATION_TYPE'(code: '$JOD_AA_ODH_STATION_CODE') data station that expose real-time sensors from 'opendatahub.bz.it'"
DESCR_LONG="This object can be used to collect data from '$JOD_AA_ODH_STATION_CODE' station (type: '$JOD_AA_ODH_STATION_TYPE') of the Alto Adige Open Data Hub's datasets."
ROOT=$(buildComponent "Root" "$MODEL" "$BRAND" "$DESCR" "$DESCR_LONG")

# Processing each device fetched
COUNT=0
echo $STATION_DATAPOINTS | jq -cr ".data.$JOD_AA_ODH_STATION_TYPE.stations.\"$JOD_AA_ODH_STATION_CODE\".sdatatypes|keys[]" | while read DATAPOINT_NAME; do
  DATAPOINT=$(echo "$STATION_DATAPOINTS" | jq -c ".data.$JOD_AA_ODH_STATION_TYPE.stations.\"$JOD_AA_ODH_STATION_CODE\".sdatatypes.\"$DATAPOINT_NAME\"")
  COUNT=$((COUNT + 1))
  NAME=$(echo "$DATAPOINT" | jq -r .tname)
  TYPE=$(echo "$DATAPOINT" | jq -r .ttype)
  UNIT=$(echo "$DATAPOINT" | jq -r .tunit)
  logInf "- adding '$NAME' station's sensors as '$TYPE' type and '$UNIT' unit"

  # Sensor's Components

  PULLER="http://requestUrl='$STATION_URL';formatType=JSON;formatPath='$.data.$JOD_AA_ODH_STATION_TYPE.stations.$JOD_AA_ODH_STATION_CODE.sdatatypes.[\\\"$DATAPOINT_NAME\\\"].tmeasurements[0].mvalue';formatPathType=JSONPATH;"
  SENSOR=$(buildComponent "$DATAPOINT_NAME" "RangeState" "puller" "$PULLER")
  #SENSOR=$(buildComponent "$DATAPOINT_NAME" "RangeState" "puller" "$PULLER" 0 100 10)

  ROOT=$(addSubComponent "$ROOT" "$SENSOR")
  tryPrettyFormat "$ROOT" >"$JOD_STRUCT" || logWar "Error on parsing component ($ROOT)"
done

logInf "John Object's structure generated for the AA OpenDataHub station '$JOD_AA_ODH_STATION_CODE' from '$JOD_AA_ODH_STATION_TYPE' dataset successfully"
